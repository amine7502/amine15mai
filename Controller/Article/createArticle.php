<?php
session_start();
$title="createArticle";
$authorId ="";
$articleTitle = "";
$body = "";
$errors = new ArrayObject();
if(isFormValid($errors)){

    $authorId = $_POST['author'];
    $articleTitle = $_POST['title'];
    $body = $_POST['body'];

    require("../Model/articleRepository.php");
    $bdd = dbConnect();
    $response = createArticle($bdd, $authorId, $articleTitle, $body);
    $lastInsertedId = $bdd->lastInsertId();
//    var_dump($lastInsertedId);
    if($lastInsertedId>0){
        header("location:../article/" . $lastInsertedId);die;
    }
    $errors->append("An error occurred, please contact your administrator system!");
}
require("../Model/UserRepository.php");
$users = getUsers();

ob_start();
displayErrors($errors);

require("../view/article/createArticleView.php");

$content=ob_get_clean();

require("../view/templateView.php");


//var_dump($_POST);die;

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('First time in the page');
        return false;
    }

    $authorId = $_POST['author'];
    $body = $_POST['body'];
    $title = $_POST['title'];

    if(!fieldsArefilled($authorId, $body, $title)){
        $errors->append('All the fields must be filled');
        return false;
    }
    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['author']) AND isset($_POST['body']) AND isset($_POST['title'])){
        return true;
    }

    return false;
}

function fieldsArefilled($authorId, $body, $title){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($authorId) OR empty($body) OR empty($title)){
        return false;
    }
    return true;
}

function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }

}




