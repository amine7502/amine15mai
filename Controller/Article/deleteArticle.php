<?php
$errors = new ArrayObject();
$title="deleteArticle";
$articleId = getArticleIdFromURI();
//var_dump($articleId);die;

require("../Model/articleRepository.php");

if(!$response = articleExist($articleId)){
    echo ("This article does not exist"); 
    exit();
}

if(isFormValid($errors)){
    //set the deletion in boolean instead of string
    $deletionConfirmed = $_POST['deletion'] == '1' ? true : false;

    if($deletionConfirmed){
        $response = deleteArticle($articleId);
        if($response){
            echo 'Article ' . $articleId . ' has been deleted successfully';
            exit();
        }

    }
    $errors->append('Article ' . $articleId . ' has not been deleted');

}

ob_start();
displayErrors($errors);

require("../view/article/deleteArticleView.php");

$content=ob_get_clean();

require("../view/templateView.php");



function getArticleIdFromURI(){
    $monUrl = $_SERVER['REQUEST_URI'];
    $monUrl = explode("/", $monUrl) ;
    $authorId = intval(end($monUrl));

    return $authorId;
}

function articleExist($articleId){
    if($articleId == 0){
        return false;
    }

    return getArticle($articleId)->fetch();
    /*
    if(getArticle($articleId)->fetch()){
        return true;
    }
    return false;*/
}

function isFormValid(ArrayObject $errors){
    if(!variablesAreSet()){
        $errors->append('Variables are not set');
        return false;
    }


    return true;
}

function variablesAreSet(){
    //Get data. If the user come directly in this page. He is redirected
    if(isset($_POST['deletion'])){
        return true;
    }

    return false;
}


function fieldsArefilled($deletionConfirmed){
    //If the user does not fill all the fields a error message is set and he is redirected
    if(empty($deletionConfirmed)){
        return false;
    }
    return true;
}


function displayErrors($errors){
    foreach ($errors as $error) {
        echo $error . '<br>';
    }
}







